#include <stdint.h>
#include <string.h>

#include "raat.hpp"

#include "comms.h"
#include "sensor.h"
#include "led.h"

void raat_custom_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices; (void)params;
	pinMode(IR_OUTPUT_PIN, OUTPUT);
	tone(IR_OUTPUT_PIN, 10000);

	sensor_setup();
	comms_setup();
}

void raat_custom_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices; (void)params;

    sensor_loop(devices, params);
    comms_loop();
    float reading = (float)sensor_get_filtered() - 100.0f;
    if (reading < 0.0f) { reading = 0.0f; }

    led_loop_distance_distance_display(devices, params, (reading*8.5f)/675.0f);
}
