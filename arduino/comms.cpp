/* C/C++ Includes */

#include <stdint.h>

/* Arduino Includes */

#include <Wire.h>

/* RAAT Includes */

#include "raat.hpp"

/* Application Includes */

#include "comms.h"
#include "location.h"

/* Defines, constants, enums, typedefs */

typedef void (*message_handler_fn)(uint8_t const * const);


/* Private Function Prototypes */

static void handle_set_index_message(uint8_t const * const msg);
static void handle_get_location_message(uint8_t const * const msg);
static void handle_get_trigger_message(uint8_t const * const msg);
static void handle_set_colours_message(uint8_t const * const msg);

/* Local Variables */

static message_handler_fn message_handler_fns[eMessageID_Count] = {
    handle_set_index_message,
    handle_get_location_message,
    handle_get_trigger_message,
    handle_set_colours_message
};

static uint8_t s_request_buffer[16];

/* Private Functions */

static uint8_t get_i2c_address()
{
    uint8_t addr = 0x50;
    
    addr |= digitalRead(ADDR_PIN_0) == LOW ? 1 : 0;
    addr |= digitalRead(ADDR_PIN_1) == LOW ? 2 : 0;
    addr |= digitalRead(ADDR_PIN_2) == LOW ? 4 : 0;
    addr |= digitalRead(ADDR_PIN_3) == LOW ? 8 : 0;

    return addr;
}

static void handle_set_index_message(uint8_t const * const msg)
{
    uint8_t index = msg[0];
    uint8_t new_location = location_set(index);
    s_request_buffer[0] = new_location;
}

static void handle_get_location_message(uint8_t const * const msg)
{
    s_request_buffer[0] = location_get();
}

static void handle_get_trigger_message(uint8_t const * const msg)
{
    (void)msg;
}

static void handle_set_colours_message(uint8_t const * const msg)
{
    (void)msg;
}

static void on_recv_handler(uint8_t recv_count)
{
    uint8_t i = 0;
    uint8_t recv_buffer[16];

    eMessageID message_id = Wire.read();
    recv_count--;

    for (; i < recv_count; i++)
    {
        recv_buffer[i] = Wire.read();
    }

    if (message_id < eMessageID_Count)
    {
        message_handler_fns[message_id](recv_buffer);
    }

    // Clear any other bytes (there shouldn't be any, but just in case)
    while (Wire.available())
    {
        (void)Wire.read();
    }
}

/* Public Functions */

void comms_setup()
{
    pinMode(ADDR_PIN_0, INPUT_PULLUP);
    pinMode(ADDR_PIN_1, INPUT_PULLUP);
    pinMode(ADDR_PIN_2, INPUT_PULLUP);
    pinMode(ADDR_PIN_3, INPUT_PULLUP);
    
    uint8_t i2c_address = get_i2c_address();
    raat_logln_P(LOG_COMMS, PSTR("I2C address: 0x%02x"), i2c_address);
    Wire.begin(i2c_address);
    Wire.onReceive(on_recv_handler);
}

void comms_loop()
{

}