#ifndef _COMMS_H_
#define _COMMS_H_

typedef enum _eMessageID
{
    eMessageID_Set_Index,
    eMessageID_Get_Location,
    eMessageID_Get_Trigger,
    eMessageID_Set_Colours,
    eMessageID_Count
} eMessageID;


void comms_setup();
void comms_loop();

#endif
