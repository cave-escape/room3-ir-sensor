#include <stdint.h>
#include <string.h>

#include <Adafruit_NeoPixel.h>

#include "raat.hpp"
#include "adafruit-neopixel-raat.hpp"

#include "raat-oneshot-timer.hpp"
#include "raat-oneshot-task.hpp"
#include "raat-task.hpp"

static AdafruitNeoPixelRAAT * pPixels;

/*static void led_debug_fn(RAATTask& thisTask, void * pTaskData)
{
    uint8_t whole_leds = ((struct distance_display_data*)pTaskData)->whole_leds;
    uint8_t fraction = ((struct distance_display_data*)pTaskData)->fraction;

    raat_logln_P(LOG_LED, PSTR("%d.%d"), whole_leds, fraction);
}
static RAATTask led_debug_task(500, led_debug_fn, NULL);*/

static void led_distance_display_fn(RAATTask& thisTask, void * pTaskData)
{
    float distance = *(float*)pTaskData;

    uint8_t whole_leds = (uint8_t)distance;
    float fraction = (distance - (float)whole_leds);

    uint8_t colours[3] = 
    {
        (255.0f * distance) / 8.5f,
        (255.0f * (8.5f - distance)) / 8.5f,
        64.0f / distance,
    };

    pPixels->clear();
    for (uint8_t i = 0; i < whole_leds; i++)
    {
        pPixels->setPixelColor(i, colours);
    }

    if (whole_leds < 8)
    {
        uint8_t last_colours[3] = 
        {
            (float)colours[0] * fraction,
            (float)colours[1] * fraction,
            (float)colours[2] * fraction
        };
        pPixels->setPixelColor(whole_leds, last_colours);       
    }

    pPixels->show();
}
static RAATTask led_distance_display_task(50, led_distance_display_fn, NULL);

void led_loop_standard(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices; (void)params;
}

void led_loop_distance_distance_display(const raat_devices_struct& devices, const raat_params_struct& params, float distance)
{
    pPixels = devices.pNeoPixels;
    led_distance_display_task.run(&distance);
}