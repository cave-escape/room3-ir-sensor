#ifndef _LED_H_
#define _LED_H_

void led_loop_standard(const raat_devices_struct& devices, const raat_params_struct& params);
void led_loop_distance_distance_display(const raat_devices_struct& devices, const raat_params_struct& params, float distance);

#endif
