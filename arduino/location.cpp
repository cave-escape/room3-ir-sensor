/* C/C++ Includes */

#include <stdint.h>

/* RAAT Includes */

#include "raat.hpp"

/* Application Includes */

#include "location.h"

/* Defines, constants, enums, typedefs */

static const uint8_t NO_LOCATION = 0xFF;

/* Local Variables */

static uint8_t s_location = NO_LOCATION;

/* Public Functions */

void location_setup()
{
    pinMode(ID_PIN_LEFT, INPUT_PULLUP);
    pinMode(ID_PIN_RIGHT, INPUT_PULLUP);
}

uint8_t location_set(uint8_t location)
{
    if (digitalRead(ID_PIN_LEFT) == LOW)
    {
        s_location = location;
        pinMode(ID_PIN_RIGHT, OUTPUT);
        digitalWrite(ID_PIN_RIGHT, LOW);
    }
    else if (digitalRead(ID_PIN_RIGHT) == LOW)
    {
        s_location = location;
        pinMode(ID_PIN_LEFT, OUTPUT);
        digitalWrite(ID_PIN_LEFT, LOW);
    }
    return s_location;
}

bool location_matches(uint8_t location)
{
    return location == s_location;
}

bool location_is_set()
{
    return s_location != NO_LOCATION;
}

uint8_t location_get()
{
    return s_location;
}
