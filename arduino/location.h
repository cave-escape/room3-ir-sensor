#ifndef _LOCATION_H_
#define _LOCATION_H_

void location_setup(uint8_t us_pin, uint8_t ds_pin);
uint8_t location_set(uint8_t location);
bool location_matches(uint8_t location);
bool location_is_set();
uint8_t location_get();

#endif
