#include <stdint.h>
#include <string.h>

#include <Ewma.h>

#include "raat.hpp"

#include "raat-oneshot-timer.hpp"
#include "raat-oneshot-task.hpp"
#include "raat-task.hpp"

static uint16_t s_last = 0U;
static Ewma s_filter(0.01);

static bool s_trigger_flag = false;

static void sensor_debug_fn(RAATTask& thisTask, void * pTaskData)
{
    (void)thisTask;
    (void)pTaskData;
    raat_logln_P(LOG_SENS, PSTR("%d (%striggered)"), s_last, s_trigger_flag ? "" : "not ");
}
static RAATTask sensor_debug_task(500, sensor_debug_fn, NULL);

void sensor_setup()
{
    pinMode(IR_INPUT_PIN, INPUT);
    (void)analogRead(IR_INPUT_PIN);
}

void sensor_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    s_last = analogRead(IR_INPUT_PIN);
    s_filter.filter(s_last);
    s_trigger_flag |= s_last > params.pIR_threshold->get();
    sensor_debug_task.run();
}

uint16_t sensor_get_reading()
{
    return s_last;
}

uint16_t sensor_get_filtered()
{
    return (uint16_t)s_filter.output;
}

bool sensor_get_trigger(bool also_clear)
{
    bool value = s_trigger_flag;

    if (also_clear)
    {
        s_trigger_flag = false;        
    }

    return value;
}
