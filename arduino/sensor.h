#ifndef _SENSOR_H_
#define _SENSOR_H_

void sensor_setup();
void sensor_loop(const raat_devices_struct& devices, const raat_params_struct& params);

uint16_t sensor_get_reading();
uint16_t sensor_get_filtered();

bool sensor_get_trigger();

#endif
